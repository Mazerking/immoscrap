#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*--------------------------*-

import unittest

from parameterized import parameterized

import Scraper as Scrap


class ScraperTest(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(ScraperTest, self).__init__(*args, **kwargs)

    def setUp(self):
        pass

    def tearDown(self):
        pass


    @parameterized.expand([
        [50, 1, "Studio 50 m"],
        [20, 1, "Studio"],
        [40, 2, "appartement"],
        [106, 5, "Appartement 5 pièces - 106 m²"],
        [106, 6, "Appartement pièces - 106 m²"],
        [4, 18, "Appartement 18 pièces - 4 m²"],

    ])
    def test_area_room(self, testarea, testroom, data):
        area, room = Scrap.find_area_and_room(data)
        testresult= [testarea, testroom]
        result = [area, room]
        self.assertEqual(testresult, result, "ok")


if __name__ == '__main__':
    unittest.main()