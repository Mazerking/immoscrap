#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import (absolute_import, division,
                        print_function, unicode_literals)

from flask import Blueprint, jsonify, g

import psycopg2.extras
import json

api = Blueprint('api', __name__)


@api.route('/geoms')
def geoms():
    # FIXME: correct the sql query
    SQL = """
        SELECT g.geom, g.cog, d.price
        FROM geo_place3 g
        JOIN (SELECT place_id, sum(price)/sum(area) as price FROM geo_place3 g LEFT JOIN appartements a ON a.place_id = g.id group by place_id) d
        ON g.id = d.place_id;"""
    # SQL = """
    # 	SELECT
    # 		ST_ASGEOJSON(geom) as geom,
    # 		cog,
    # 		sum(a.price)/sum(a.area) as price from geo_place2 g JOIN appartements a ON a.place_id = g.id group by g.cog;
    # 	"""
    cursor = g.db.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute(SQL)

    geoms = {
        'type': 'FeatureCollection',
        'features': []
    }
    for row in cursor:

        strrow = row[0].replace("\'", "\"")
        jsoned = json.loads(strrow)
        jsoned2 = json.dumps(jsoned['geometry'])
        if not row[0]:
            continue
        geometry = {
            'type': 'Feature',
            'geometry': json.loads(jsoned2),
            # 'geometry': json.loads(row['geom']),
            'properties': {"cog": row['cog'],
                           "price": row['price']}
        }
        geoms['features'].append(geometry)
    return jsonify(geoms)


def get_list_appart(cog,price_jump,xtime):
    '''

    :param cog:
    :param price_jump:
    :param xtime:
    :return: List of int
    '''
    price_init = 0
    next_price = price_jump
    listresult = []
    for i in range(1, xtime):
        SQL = """SELECT COUNT(a.listing_id)
                from geo_place3 g
                JOIN appartements a ON a.place_id = g.id 
                where g.cog = {0} AND a.price BETWEEN {1} AND {2};""".format(str(cog), str(price_init), str(next_price))
        cursor = g.db.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cursor.execute(SQL)
        for row in cursor:
            listresult.append(row[0])
        price_init += price_jump
        next_price += price_jump

    SQL = """SELECT COUNT(a.listing_id)
                    from geo_place3 g
                    JOIN appartements a ON a.place_id = g.id 
                    where g.cog = {0} AND a.price > {1};""".format(str(cog), str(next_price))
    cursor = g.db.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute(SQL)
    for row in cursor:
        listresult.append(row[0])

    return listresult

@api.route('/get_price/<path:cog>')
def get_price(cog):
    """
    Return the volumes distribution for the given cog in json format
    """
    # FIXME
    #   - Give a better name
    #   - correct the volumes (nb annonces) / labels fields (prix tout les 100k)

    list_result = get_list_appart(cog, 100000, 19)
    serie_name = 'Annonces par fourchette de prix ' + cog
    volumes = list_result
    labels = ['0 - 100K', '200K - 300K', '300K - 400K', '400K - 500K', '500K - 600K', '600K - 700K', '700K - 800K',
              '800K - 900K', '1M - 1.1M', '1.1M - 1.2M', '1.2M - 1.3M', '1.3M - 1.4M', '1.4M - 1.5M', '1.5M - 1.6M',
              '1.6M - 1.7M', '1.7M - 1.8M', '1.8M - 1.9M', '1.9M - 2M', '2M+']

    response = {
        'serie_name': serie_name,
        'volumes': volumes,
        'labels': labels
    }
    return jsonify(response)
