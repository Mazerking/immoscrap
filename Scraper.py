#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import lxml.html
from fake_useragent import UserAgent
import re
import pandas as pd


"""
Compile regex beforehand for better performance
"""
regex_find_area = re.compile(r"\d{1,5}\sm")
regex_find_Studio = re.compile(r"Studio")
regex_find_room = re.compile(r"\d{1,2}")


def create_doc(url):
    '''

    :return:
    '''
    try:
        ua = UserAgent()
        headers = {"User-Agent": ua.random}
        html = requests.get(url, headers=headers)
        doc = lxml.html.fromstring(html.content)

    except requests.exceptions.RequestException as e:
        print(e)
        return None
    return doc


def get_listing_id(announce, doc):
    get_listing_id = doc.xpath(
        '//*[@id="page-wrapper"]/div[3]/div/div[3]/section[1]/div/div[2]/div[' + str(announce) + ']/div/a/@href')
    listing_id = int(str(get_listing_id[0]).replace("https://www.meilleursagents.com/annonces/achat/", "")[:-1])
    return listing_id


def get_price(announce, doc):
    get_price = doc.xpath(
        '//*[@id="page-wrapper"]/div[3]/div/div[3]/section[1]/div/div[2]/div[' + str(
            announce) + ']/div/a/div[2]/text()')
    if str(get_price[0].strip()) == "Prix non communiqué":
        # Vrai prix moyen 980 000€
        area, room = get_area_and_room(announce, doc)
        if area < 50:
            return 333335
        elif area < 70:
            return 980000
        else:
            return 1500000
    try:
        price = int(str(get_price[0]).replace(" ", "").strip()[:-2])
    except ValueError as e:
        # to improve !
        print(e)
        return 333333
    return price


def get_area_and_room(announce, doc):
    get_title = doc.xpath(
        '//*[@id="page-wrapper"]/div[3]/div/div[3]/section[1]/div/div[2]/div[' + str(
            announce) + ']/div/a/div[1]/text()')
    area, room = find_area_and_room(get_title[0].strip())
    return area, room


def find_area_and_room(title):
    """
    search in string Area and room
    :return: int, int
    """
    match = regex_find_area.search(title)
    if match is None:
        # if there is no area in title, we multiply area by 15 for now
        room_match = regex_find_room.search(title)
        if room_match is None:
            # To improve
            if regex_find_Studio.search(title):
                return 20, 1
            return 40, 2
        room = int(room_match.group(0))
        area = room * 15
        return area, room
    area = int(match.group(0)[:-2])
    if regex_find_Studio.search(title):
        room = 1
    else:
        room_match = regex_find_room.search(title.replace(match.group(0), ""))
        if room_match is None:
            # To improve allow 1 area each 20meters
            room = 1
            number = 20
            while number < area:
                number += 20
                room += 1
            return area, room
        room = int(room_match.group(0))
    return area, room


def write_in_json(dicvalue):
    df = pd.DataFrame(dicvalue)
    out = df.to_json(orient='records')
    with open('Result/result_json.json', 'w+') as f:
        f.write(out)


if __name__ == "__main__":
    arronList = [32682, 32683, 32684, 32685, 32686, 32687, 32688, 32689, 32690, 32691, 32692, 32693, 32694, 32695, 32696, 32697, 32698, 32699, 32700, 32701]
    dicValue = {'place_id': [], 'listing_id': [], 'price': [], 'area': [], 'room_count': []}
    for arron in arronList:
        url = "https://www.meilleursagents.com/annonces/achat/search/?item_types=ITEM_TYPE.APARTMENT&place_ids=" + str(arron)
        doc = create_doc(url)
        if doc is None:
            print("Error with " + url)
            continue
        max_page = int(doc.xpath(
            '//*[@id="page-wrapper"]/div[3]/div/div[3]/section[1]/div/div[3]/ul/li[6]/a/@data-paginate-page-num')[0])

        for page in range(1, max_page + 1):
            url_page = url + "&page=" + str(page)
            doc = create_doc(url_page)
            if doc is None:
                print("Error with " + url)
                continue
            total_announce = len(doc.xpath('//*[@class="listing-item__content"]'))
            for announce in range(1, total_announce + 1):
                listing_id = get_listing_id(announce, doc)
                price = get_price(announce, doc)
                area, room = get_area_and_room(announce, doc)

                dicValue['place_id'].append(arron)
                dicValue['listing_id'].append(listing_id)
                dicValue['price'].append(price)
                dicValue['area'].append(area)
                dicValue['room_count'].append(room)

    write_in_json(dicValue)
