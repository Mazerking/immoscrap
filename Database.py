#!/usr/bin/env python
# -*- coding: utf-8 -*-

import psycopg2 as py
from dbdrivers import AbstractDriver
import pandas as pd


class PostgreSQLDriver(AbstractDriver):
    def connect(self, host, port, user, passwd, db):
        try:
            self.database = db
            connect_str = "dbname='{}' user='{}' host='{}' password='{}' port='{}'" \
                .format(db, user, host, password, port)
            self.conn = py.connect(connect_str)
            self.cur = self.conn.cursor()
        except py.Error as e:
           print(e)

    def disconnect(self):
        try:
            self.cur.close()
            self.conn.close()
        except py.Error as e:
            print(e)

    def return_query(self, query):
        self.cur.execute(query)
        result = self.cur.fetchall()
        return result

    def exec_query(self, query):
        self.cur.execute(query)
        self.conn.commit()


if __name__ == '__main__':
    db_driver = PostgreSQLDriver()
    db = "meilleursagents"
    user = "meilleursagents"
    password = "pikachu42!@"
    host = "localhost"
    port = 5432
    db_driver.connect(host, port, user, password, db)

    data = pd.read_json(r'Result/result_json.json')
    for listing_id in data['listing_id']:

        query = "select * from appartements Where listing_id = {0}".format(str(listing_id))
        announce_in_base = db_driver.return_query(query)
        announce_data = data.loc[data['listing_id'] == listing_id].iloc[0]

        if len(announce_in_base) != 0:
            # if announce exist in database
            if announce_data['price'] != announce_in_base[0][2]:
                # Write in historic then update
                query = "INSERT INTO historic VALUES ({0}, {1})".format(str(listing_id), str(announce_in_base[0][2]))
                db_driver.exec_query(query)
                query = "UPDATE appartements SET price = {0} WHERE listing_id = {1}".format(
                    str(announce_data['price']), str(listing_id))
                db_driver.exec_query(query)
            query = "UPDATE appartements SET last_seen = CURRENT_DATE WHERE listing_id = {0}".format(str(listing_id))
            db_driver.exec_query(query)
        else:
            query = "INSERT INTO appartements VALUES ({0}, {1}, {2}, {3}, {4})".format(str(listing_id), str(announce_data['place_id']), str(announce_data['price']), str(announce_data['area']), str(announce_data['room_count']))
            db_driver.exec_query(query)
