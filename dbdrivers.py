#!/usr/bin/env python
# -*- coding: utf-8 -*-
from abc import ABC, abstractmethod


class AbstractDriver(ABC):
    def __init__(self):
        self.database = None
        self.conn = None
        self.cur = None

    @abstractmethod
    def connect(self, host, port, user, passwd, db):
        """
        This function enable a connection to a database
        :param host: The name of the host you want to connect to.
        :param port: The port number of the database
        :param user: The username which has a FULL read access to the database
        :param passwd: The password of the user.
        :param db: The database name.
        :return: None
        """
        pass

    @abstractmethod
    def disconnect(self):
        """
        This function stop the connection between client and database.
        :return: None
        :raise: DatabaseError if their is an error during the connection to the database
        """
        pass
